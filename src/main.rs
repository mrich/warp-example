use mobc::{Connection, Pool};
use mobc_postgres::{tokio_postgres, PgConnectionManager};
use tokio_postgres::NoTls;
use warp::{http::StatusCode, Filter};

mod db;
mod error;

type DBCon = Connection<PgConnectionManager<NoTls>>;
type DBPool = Pool<PgConnectionManager<NoTls>>;

#[tokio::main]
async fn main() {
    let health_route = warp::path!("health").map(|| StatusCode::OK);
    let routes = health_route.with(warp::cors().allow_any_origin());
    warp::serve(routes).run((([127, 0, 0, 1]), 8001)).await;
}
