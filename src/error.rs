use mobc_postgres::tokio_postgres;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error("error getting connection from the DB pool: {0}")]
    DBPoolError(mobc::Error<tokio_postgres::Error>),
}
