use std::{str::FromStr, time::Duration};

use crate::{DBCon, DBPool};
use mobc::{Error, Pool};
use mobc_postgres::{
    tokio_postgres::{self, error::DbError, Config, NoTls},
    PgConnectionManager,
};
use tokio_postgres::Error;

const DB_POOL_MAX_OPEN: u64 = 32;
const DB_POOL_MAX_IDLE: u64 = 8;
const DB_POOL_TIMEOUT_SECONDS: u64 = 15;

pub fn create_pool() -> std::result::Result<DBPool, mobc::Error<Error>> {
    let config = Config::from_str("postgres://postgres@127.0.0.1:7878/postgres")?;
    let manager = PgConnectionManager::new(config, NoTls);
    Ok(Pool::builder()
        .max_open(DB_POOL_MAX_OPEN)
        .max_idle(DB_POOL_MAX_IDLE)
        .get_timeout(Some(Duration::from_secs(DB_POOL_TIMEOUT_SECONDS)))
        .build(manager))
}

const INIT_SQL: &str = ".db.sql";

pub async fn get_db_conn(db_pool: &DBPool) -> Result<DBCon, String> {
    db_pool.get().await.map_err(DbError)
}

pub async fn init_db(db_pool: &DBPool) -> Result<(), String> {}
